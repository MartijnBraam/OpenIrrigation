#!/usr/bin/env python
# -*- coding: UTF-8 -*-
version = '2.0'

import cgi
import jinja2

loader = jinja2.FileSystemLoader("/opt/OpenIrrigation/templates")
render_env = jinja2.Environment(loader=loader)

form = cgi.FieldStorage()
ClientId = form.getvalue('ClientId', '0000')
SSID = form.getvalue('SSID', '0')
Passkey = form.getvalue('Passkey', '0')
IP = form.getvalue('IP', '0')
IPR = form.getvalue('IPR', '0')

template = render_env.get_template('networksettings.html')
html = template.render(ip=IP, ipr=IPR, version=version)

print('Content-type:text/html\r\n\r\n')
print(html)

# upload data to textfile for use by htmlgenerator
output = open('/OpenIrrigation/network.txt', 'w')
output.write(ClientId + '\n')
output.write(SSID + '\n')
output.write(Passkey + '\n')
output.write(IP + '\n')
output.write(IPR + '\n')
# check if an static ip is configured:
if IP > '0':
    output.write('static\n')
else:
    output.write('dhcp\n')
output.close()
