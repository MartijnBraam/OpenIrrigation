[HOME](README.md) [HARDWARE](PARTS.md) [INSTALLATION](INSTALLATION.md) [USER-INTERFACE](WEBINTERFACE.md)
<br><br>
# Raspberry installation
For simple installation instructions on installing the OS on your raspberry py refer to the
<a href="https://www.raspberrypi.org/documentation/installation/installing-images/">installation guide</a> 
on the rapsberry manufacturers webpage.

# Configure the raspberry
To configure the raspberry for its first use hook up a keyboard via it's USB port and a screen (TV) via the HDMI port.<br>
A simple configuration tool is available via the command:
```
sudo raspi-config
```
Enter the Network options and configure the LAN / WLAN with a static IP address.<br>
For more information refer to the 
<a href="https://www.raspberrypi.org/documentation/configuration/raspi-config.md">configuration guide</a> on the rapsberry manufacturers webpage.

# Files and dependencies
Before using OpenIrrigation the following software should be installed:
```
sudo apt install apache2 python3-pip
sudo a2enmod cgi
sudo apt-get install git-core
git clone git://git.drogon.net/wiringPi
cd ~/wiringPi
git pull origin
./build
cd
git clone https://github.com/jtdaling/OpenIrrigation.git
cp OpenIrrigation/* /home/pi/
sudo chmod 222 /sys/class/gpio/export /sys/class/gpio/unexport 
echo "11" > /sys/class/gpio/export
echo "13" > /sys/class/gpio/export
echo "14" > /sys/class/gpio/export
echo "17" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio11/direction
echo "out" > /sys/class/gpio/gpio13/direction
echo "out" > /sys/class/gpio/gpio14/direction
echo "out" > /sys/class/gpio/gpio17/direction
```

Edit the file /etc/apache2/sites-available/httpd.conf with your favorite text editor and add:
```
ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
<Directory "/usr/lib/cgi-bin">
AllowOverride None

Options Indexes FollowSymLinks ExecCGI
AddHandler cgi-script .cgi .py
Order allow,deny

Allow from all
</Directory>
```
Go or create a directory and clone the current project files to your computer (raspberry)
```
git clone https://github.com/jtdaling/OpenIrrigation
```
Copy the file form.py to the /usr/lib/cgi-bin/ directory and issue the following command:
```
sudo cp form.py /usr/lib/cgi-bin/form.py
sudo chown pi:pi /usr/lib/cgi-bin/form.py
sudo chmod 755 /usr/lib/cgi-bin/form.py
```
Create a schedule to run the scripts
```
crontab -e
```

Add the following lines in the file:<br>
*change /home/pi/ if you are using a different username or folder*

```
00      *     *     *     *         /usr/bin/python /home/pi/timecontroller.py
00      *     *     *     *         /usr/bin/python /home/pi/raininfo.py
00      04    *     *     *         /usr/bin/python /home/pi/raininforesult.py

```
Install python dependencies:
```
pip install requests RPi.GPIO pathlib
```
run runonce.py and form.py once to create a blank website.
```
sudo python runonce.py
sudo python form.py
```
Set permissions for the website.
```
sudo chown pi:pi /var/www/html/*
sudo chmod 755 /var/www/html/*
```
If installed, allow port 80 in ufw.
```
sudo chown pi:pi /var/www/html/*
sudo chmod 755 /var/www/html/*
```
## Settings
Go to the Raspberry's url and click on settings.

<img src="images/settings.png" alt="settingsscreen" class="inline"/>
<br>
Fill in all necessary information about your garden or field.
<br><br>
**The following fields are mandatory for the software to function:**<br>
 sprinkler or drip system<br>
 location<br>
 Wunderground API key<br>
<br>
A Wunderground API key can be ordered via the following <a href="https://www.wunderground.com/weather/api/d/pricing.html">link</a>.<br>
Press the store button and leave the browser page open.
<br><br>

**The following workaround is necessary to enable form.py to access files in de home directory,<br>
any suggestions to improve this fix are welcome!**<br>
run the following commands
```
sudo chmod 777 /var/www/html/gardendata.html 
sudo chmod 777 gardendata.txt
```
In your browser press the back button to view your settings and press store again.<br>
Run raininfo.py and raininforesult.py once to create the necessary databases for the software to function.
<br><br>
```
python raininfo.py
python raininforesult.py
```

<br><br>
For more information about using the web interface refer to the [web interface page](WEBINTERFACE.md). 
<br><br>
[HOME](README.md) [HARDWARE](PARTS.md) [INSTALLATION](INSTALLATION.md) [USER-INTERFACE](WEBINTERFACE.md)
