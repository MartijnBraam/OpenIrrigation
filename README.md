[HOME](README.md) [HARDWARE](PARTS.md) [INSTALLATION](INSTALLATION.md) [USER-INTERFACE](WEBINTERFACE.md)
<br><br>
# OpenIrrigation
Dear visitor,<br>
<br>
thank you very much for your interest in the OpenSprinkler project.<br>
The project that aims to reduce water consumption of sprinkler systems based on actual temperatures and forcasted weather conditions.<br>

After one year of real life testing and tweaking, OpenIrrigation has undergone many improvements and is released as version 2.<br>
An image file has been created for ease of use.<br>
After writing the image to a SD card (https://www.raspberrypi.org/documentation/installation/installing-images/), the system will create a WLAN access point at first start.<br>
To configure the system you connect to OpenIrrigation wlan, lauch the browser and visit the address: 192.168.1.1 and fill in all the network details. <br>
After a restart the system is ready to be used.<br>
If the configuration of the network goes wrong, the system will reset itselve and the OpenIrrigation wlan will be created again.<br>

The software is developed to turn a Raspberry Pi into an irrigation computer.<br>
It can be used for gardens but the intention is to develop it further for agricultural use.<br>
Aim is to improve irrigation efficiency by reducing water spillage and minerals lost by over-irrigating.<br>
This will not only result in environmental benefits but will also lead to a better plant health and growth improvement.<br>
<br>
The descriptions are based on a demo setup which runs at my home.<br>
A fresh installation will be empty and you will have to configure your own garden and valve usage before the software can control your irrigation valves.<br>
Configuring will take a few minutes only.<br>
In the demo setup, the raspberry will connect to a circuit board with 4 relais which will power up to 4 irrigation valves.<br>
I have used 24V Rainbird irrigation valves in combination with a Rainbird Transformer.<br>
<br>
Please refer to the [parts page](PARTS.md) for an overview of parts I used.<br>
The total costs for all the parts where around 100 euro's.<br>
<br>
**Don't buy 9V valves since they need an inverse current to close**

# Control
The user controls the irrigation computer via a browser.<br>
Via the raspberry's IP addres a website will show water usage for the current day, the past 4 weeks and the current year.<br><br>

<img src="images/mainscreen.png" alt="mainscreen" class="inline"/>

<br>
Configuring valves and field data is done via the settings page.<br>
Up to 4 valves can be configured and the field data is also used for fertilizing planning.<br><br>

<img src="images/settings.png" alt="settingsscreen" class="inline"/>

# Installation
Please refer to the [installation page](INSTALLATION.md) for detailed installation instructions.<br>
The project files can be downloaded to the pi's home directory.<br>
The software is written in python and uses apache to enable web access.<br>
If you run into problems installing, don't hessitate to contact me.<br>
The scripts have been thoroughly tested and has been running on my raspberry for a few months before the scripts where uploaded.<br>
The installation manual might not be complete so any feetback is welcome!

# Hardware
Please refer to the [hardware page](PARTS.md) for detailed hardware purchase and installation instructions.<br>
The hardware I used consists of a raspberry 3, a scandisk Extreme SD card, a simple 3,3V relaisboard, waterresistant casing
and Rainbird irrigation valves, pipes and sprinklers.<br>
I am using the system to water my lawns, perennial border, balcony geraniums and composter.<br>
The relais handles voltages up to 230V which also allows an irrigation pump to be connected.<br>
The software uses weatherdata provided from weather underground.<br>
The actual location is entered in de settings page.<br>
The software doesn't support weather sensors at this moment.<br>


# Operation
The software runs 7 times per day.<br>
It makes a distinguish between sprinklers and drip irrigation systems.<br>

## Irrigation time
The software comes with a plant database for the most common plants.<br>
The plant database is far from complete and will be contuously updated.<br>
The database provides information about plant growth and water needs based on the average day temperature.<br>
Goal is to provide the roots with sufficient water up to a depth of 5-10 cm.<br>
The upper roots are responsible for the plant's water needs and deeper roots serve mostly plant's strength.<br>
If to much water is given, the plant will not be able to absorb it and excess water will drain into the groundwater flushing
minerals with it.<br>
Overwatering also leads to erosion further limiting plant growth and production.<br><br>

The software will calculate the plant's water needs by interpolating a growth curve.<br><br>

<img src="images/photosynthesis.png" alt="photosynthesis" class="inline"/>
<br><br>

It will then look at the wind conditions. If the wind exceeds the maximum wind value provided in the settings page
it will cancel the irrigation and try again in the evening.<br>
<br>
If there was a water shortage in the past 7 days due to wind conditions or rapidly changing temperatures,
the software will compensate the shortage over the next 7 days.<br>
This also counts for a surplus.<br>
After the optimum amount of irrigation has been calculated the software will open the respective valve for a period based on
the systems water output provided in the settings page.<br>
When the irrigation process finishes the software will write details to a database.<br>
Importand information such as todays irrigation amount, today's irrigation progress and irrigation amounts for the past weeks
and current year can be viewed via the irrigation computer's website.<br>
The website will show warnings when the minimum temperatures are close to freezing.<br><br>
If a valve is not in use, the valve can be switched off in the settings page.<br>
All other settings stay saved for future use.<br>

## Sprinkler
The sprinkler will preferably irrigate before sunrise (05:00 AM).<br>
This will avoid leave sunburn due to waterdroplets and it will contribute to the irrigation efficiency since water 
is added just prior to daylight when plants need water for their growth.<br>

## Drip system
The drip system will water 5 times per day.<br>
It will start at 08:00 am and end at 18:00 pm.<br>
A drip system is more efficient than a sprinkler system providing water when it is most needed at a low rate.<br>
<br><br>
[HOME](README.md) [HARDWARE](PARTS.md) [INSTALLATION](INSTALLATION.md) [USER-INTERFACE](WEBINTERFACE.md)
