
[HOME](README.md) [HARDWARE](PARTS.md) [INSTALLATION](INSTALLATION.md) [USER-INTERFACE](WEBINTERFACE.md)
<br><br>
# Parts
The parts needed are listed below.<br>
They can be easily ordered via sites like Ebay or Amazon.<br>

- Raspberry Pi 3 Model B Motherboard
- raspberry power supply
- female to female jumper wires
- 4 channel dc relay board
- 2 waterproof junction boxes
- a 2 or 3 outlet power strip

If you are planning to use the same hardware as me:<br>

- a 24V AC transformer<br>
*don't use a 9V system since this systems requires an inverse current to close the valve*
- 6 wire (ground) cable
- wire protector pipe
- up to 4 (Rainbird) irrigation valves
- pressure regulators for drip systems
- (Rainbird VBREC12) valve box
- polyethylene tubing with connectors, elbows etc.
- Sprinklers
- drip emitters

# Putting it all together

In order to prevent overheating, I kept the transformators in a seperate junction box.<br>

<img src="images/powercase.png" alt="powerjuntionbox" class="inline"/>

The raspberry pi and relais board are build into the second junction box.<br>
I used some materials found in a hardware store to construct a frame that supports the relays board above the raspberry.<br>

<img src="images/raspberrycase.png" alt="raspberryjuntionbox" class="inline"/>

The jumper cables provide electric signals from the raspberry to the relais board.<br>

<img src="images/gpiopinsv3withpi.png" alt="raspberrypi3gpiopins" class="inline"/>

On the relais board there are 6 pins located in the lower right corner.<br>
Use jumper wires for the following connections:<br>

- GND is connected to any GND on the raspberry
- VCC is connected to pin 1 on the raspberry
- in1 is connected to GPIO11 (pin 23) on the raspberry
- in2 is connected to GPIO13 (pin 33) on the raspberry
- in3 is connected to GPIO17 (pin 11) on the raspberry
- in4 is connected to GPIO14 (pin 8) on the raspberry

When power is supplied to the raspberry all 4 (red) LED's should light.<br>
<br><br>
[HOME](README.md) [HARDWARE](PARTS.md) [INSTALLATION](INSTALLATION.md) [USER-INTERFACE](WEBINTERFACE.md)
